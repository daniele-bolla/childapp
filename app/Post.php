<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $fillable = [
      'tit', 'txt','user_id'
  ];

	public function tags (){
		return $this->belongsToMany('App\Tags')->withTimestamps();
	}	
	
	public function user (){
		return $this->belongsTo('App\User');
	}
}
