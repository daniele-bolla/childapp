<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array 
     */
    protected $fillable = [
       'id', 'name', 'email', 'password','role'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      
        'password', 'remember_token',
      
    ];
  
    public function posts (){
      
      return $this->hasMany('App\Posts');
      
    }	
  
    public function role(){
      
      return $this->role;
      
    }
  
   /* public function admin() {
      
        //return $this->role;
      if($this->role === 10) {
        
        return true;
        
      } else {
        
        return false;
      }
      
    }    
  
    public function editor() {
      
        //return $this->role;
      if($this->role == 100) {
        
        return true;
        
      } else {
        
        return false;
        
      }
      
      
    }
    */
  
}
