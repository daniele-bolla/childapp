<?php

namespace App\Http\Middleware;

use Auth;

use Closure;

class editor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        
          if(Auth::user()->role === "100"){
           
            return response()->json(['error' => 'You are not authorized for this '], 401);
          
          }

        return $next($request);
    }
  
}
