<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Hash;
use Input;
use Validator;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use AuthenticatesAndRegistersUsers, ThrottlesLogins;

class authController extends Controller
{
	
  public function __construct() 
	{
		
	 	$this->middleware('jwt.auth', ['except' => ['login','store']]);	 	
		
		$this->middleware('admin', ['only' => ['update']]);
		
		$this->middleware('editor', ['only' => ['show']]);
		
   }
  
  
  public function index()
	{
		
		return User::all();
		
  }
  
  public function login(Request $request)
  {
		
      $credentials = $request->only('email', 'password');
		
      try {
          // verify the credentials and create a token for the user
          if (! $token = JWTAuth::attempt($credentials)) {
						
              return response()->json(['error' => 'invalid_credentials'], 401);
          }
				
      } catch (JWTException $e) {
          // something went wrong
          return response()->json(['error' => 'could_not_create_token'], 500);
      }
      // if no errors are encountered we can return a JWT
      return response()->json(compact('token'));
		
  }
  
  public function store(Request $request){
		
			$validator = Validator::make($request->all(), [

				'name' => 'required',

				'email'      => 'required|email|unique:users,email',

				'password'   => 'required|confirmed',
				//regex:pattern|min:8|max:32
				'password_confirmation' => 'required'

			]);

			if ($validator->fails()) {

				return $validator->errors();

			} else  {

				$user = $request->all();
				
				$user['role'] = 10;

				$user['password'] =  Hash::make($user['password']);

				User::create($user);

				//return $user;
				
			}
		
  }
  
  public function show($id, Request $request)
  {
		
    $user = User::findOrFail($id);
		
    //$user['posts'] = $user->posts()->lists('tit');
		
    return $user;
		
  }
	
	public function update ($id, Request $request)
  {
		
		$user = User::findOrFail($id);
		
		$data = $request->all();
		
		$validator = Validator::make($data, [

			'name' => 'required',

			'email' => 'required|email|unique:users,email,'.$id 

		]);

		if ($validator->fails()) {
			
 			return $validator->errors();

		} else  {

			$data['updated_at'] = Carbon::now();

			$user->update($data);

			return response()->json(['success' => 'user updated'], 200);

		}

  }	  
	
	public function logged()
	{
			try {

					if (! $user = JWTAuth::parseToken()->authenticate()) {
							return response()->json(['user_not_found'], 404);
					}

			} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

					return response()->json(['token_expired'], $e->getStatusCode());

			} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

					return response()->json(['token_invalid'], $e->getStatusCode());

			} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

					return response()->json(['token_absent'], $e->getStatusCode());

			}
			// the token is valid and we have found the user via the sub claim
			return response()->json(compact('user'));
		
	}
	
	public function check(Request $request)
  {
		
		$validator = Validator::make($request->all(), [
			
			'email'      => 'required|email|unique:users,email',
			
		]);

		if ($validator->fails()) {
			
			$errors = $validator->errors();
			
			return response()->json($errors, 500);

		} else  {
			
			return true;
			
		}
    
  }			
	
}