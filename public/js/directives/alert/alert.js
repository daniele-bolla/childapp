(function() {
'use strict'; 
angular.module('app')


.directive('alert', [ function() {
  return {
    
    restrict: 'E',

    replace: true,
    
    scope: {data:'='},

    
    templateUrl: 'js/directive/jalert.html'
  }
}])
  
})();