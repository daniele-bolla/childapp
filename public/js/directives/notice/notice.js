(function() {

  'use strict';

  angular.module('app')

  .directive('notice', ['$rootScope', function($rootScope) {

    return {

      restrict: 'E',
      
      templateUrl: 'js/directives/notice/notice.tpl.html'

    };
    
  }]);

})();