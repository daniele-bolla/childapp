(function() {

  'use strict';

  angular.module('app')

  .directive('navbar', ['$rootScope', function($rootScope) {

    return {

      restrict: 'E',

      scope: {

        brand: '@',

        styles: "@"

      },

      link: function(s) {

        s.menus = [

          {

            'txt': 'Users',

            'url': '#/users',

          },

          {
            'txt': 'Posts',

            'url': '#/posts'

          },

        ];

      },

      templateUrl: 'js/directives/navbar-top/navbar-top.tpl.html'

    };

  }]);

})();