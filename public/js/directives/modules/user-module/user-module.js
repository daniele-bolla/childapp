(function() {

  'use strict';

  angular.module('app')

  .directive('userModule', ['$rootScope','userService', function($rootScope, userService) {

    return {

      restrict: 'E',
      
      controllerAs: 'us',
      
      templateUrl: 'js/directives/modules/user-module/user-module.tpl.html'

    };
      
  }]);

})();