  (function() {

    'use strict';

    angular
      .module('app')
      .directive('loginModule', ['authService','$auth', function(authService, $auth) {
        return {
          restrict: 'E',
          replace: true,

          scope: {
            head: '@',
            style: '@'
          },

          templateUrl: 'js/directives/modules/login-module/login-module.tpl.html',

          /** Logic directive **/
          controller: function($scope,$auth) {
            $scope.authenticate = function(provider) {
              $auth.authenticate(provider);
            };

            $scope.action = authService.login;
            $scope.form = {
              buttons: [{
                'txt': 'Login',
                'style': ' btn-success'
              }]
            }

          },
          controllerAs: 'log',
          bindToController: true

        };
      }]);

  })();