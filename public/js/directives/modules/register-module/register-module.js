(function() {

  'use strict';

  angular.module('app')

  .directive('registerModule', ['authService', function(authService) {

    return {

      restrict: 'E',

      replace: true,

      scope: {

        head: '@',

        style: '@'

      },

      templateUrl: 'js/directives/modules/register-module/register-module.tpl.html',
      
      /** Logic directive **/
      controller: function($scope, $element, $http) {
        
        /*Action*/
        $scope.action = authService.signup;
        
        /*$watch method for binding from authService*/
        $scope.$watch(function() {
          
          return authService.regAlerts
          
        }, function(data) {

          $scope.alerts = authService.regAlerts;

          $scope.msg = authService.regErrors;

        }, true);

        /** Popula directive **/
        $scope.form = {

          buttons: [{

            'txt': 'Sign Up',

            'style': ' btn-info btn-block'

          }]

        }

      },

      controllerAs: 'reg',

      bindToController: true,

    };

  }]);

})();