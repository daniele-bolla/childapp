(function() {

  'use strict';

  angular.module('app')

  .directive('PassResetModule', ['$rootScope', function($rootScope) {

    return {

      restrict: 'E',
      
      templateUrl: 'js/directives/pass-reset-module/pass-reset-module.tpl.html'

    };
    
  }]);

})();