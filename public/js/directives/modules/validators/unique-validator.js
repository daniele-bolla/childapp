(function() {

  'use strict';

  angular
    .module('app')
    .directive('uniqueValidator', ['$http', function($http) {
       return {
         require: 'ngModel',
         restrict: 'A',
         link: function(scope, elem, attrs, reg) {
           elem.on('blur', function(evt) {
               scope.$apply(function() {
                 $http({
                   method: 'POST',
                   url: 'public/api/check',
                   data: {
                     username: elem.val(),
                    // dbField: attrs.ngUnique
                   }
                 }).success(function(data, status, headers, config) {
                   //reg.$setValidity('unique', data.status);
                   console.log(data);
                 });
               });
             })
           }
         }
    }]);

})();