(function() {

  'use strict';

  angular
    .module('app')
    .directive('macthValidator', function() {
      return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, elm, attrs, reg) {
          reg.$parsers.unshift(function(viewValue, $scope) {
            var noMatch = viewValue != scope.regForm.password.$viewValue
            reg.$setValidity('noMatch', !noMatch)
          })
        }
      }
    });
  
})();