  (function() {

    'use strict';

    angular.module('app')

    .directive('userMenu', ['$rootScope', function($rootScope) {

      return {

        restrict: 'E',

        replace: true,

        scope: false,

        templateUrl: 'js/directives/user-menu/user-menu.tpl.html'

      };

    }]);

  })();