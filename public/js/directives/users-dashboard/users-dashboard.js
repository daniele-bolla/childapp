(function() {

  'use strict';

  angular.module('app')

  .directive('userDashboard', ['$rootScope','userService', function($rootScope, userService) {

    return {

     restrict: 'E',

      /* 
      
      scope: {

        brand: '@',

        styles: "@"

      },
      
      */
      
      controller: function($scope) {
     
       userService.getList();
        
       $scope.action =  userService.show;
        
       $scope.update =  userService.update;
        
       $scope.$watch( function () { return userService.list}, function (data) {      
          
            $scope.users = data;
         
          }, true); 
       
       $scope.$watch( function () { return userService.user }, function (data) {
          
            $scope.u = data;
         
          }, true);
        
       $scope.$watch( function () { return userService.alerts}, function (data) {      
          
            $scope.alerts = data;
         
          }, true); 
         
      },
    
      templateUrl: 'js/directives/users-dashboard/users-dashboard.tpl.html'

    };

  }]);

})();