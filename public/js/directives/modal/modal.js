(function() {

  'use strict';

  angular.module('app')

  .directive('Modal', ['$rootScope', function($rootScope) {

    return {

      restrict: 'E',
      scope: {},
      transclude: true,
      templateUrl: 'js/directives/modal/modal.tpl.html'

    };
    
  }]);

})();