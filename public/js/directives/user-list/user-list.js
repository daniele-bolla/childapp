(function() {

  'use strict';

  angular.module('app')

  .directive('userList', ['$rootScope','userService', function($rootScope, userService) {

    return {

      restrict: 'E',

      templateUrl: 'js/directives/user-list/user-list.tpl.html'

    };

  }]);

})();