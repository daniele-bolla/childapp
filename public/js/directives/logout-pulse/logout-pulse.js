  (function() {

    'use strict';

    angular
      .module('app')
      .directive('logoutPulse', ['authService', function(authService) {
        return {
          restrict: 'E',
          replace: true,

          scope: {
            head: '@',
            style: '@'
          },

          templateUrl: 'js/directives/logout-pulse/logout-pulse.tpl.html',

          /** Logic directive **/
          controller: function($scope) {

            $scope.action = authService.logout;
            $scope.button = {

                'txt': 'Logout',
                'style': ' btn-warning'

            }

          },
          controllerAs: 'out',
          bindToController: true

        };
      }]);

  })();