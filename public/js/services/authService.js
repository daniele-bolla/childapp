(function() {

  'use strict';

  angular.module('app')

  .service('authService', function($rootScope, $http, $log, $auth, $location) {

    var root = this;
    
    root.regErrors = false;
    
    root.regAlerts = {"asd": "asd"}    
    
    root.logErrors = false;
    
    root.logAlerts = {"asd": "asd"}

    if (localStorage.getItem('user')) {
      
      var u = localStorage.getItem('user');
      
      $rootScope.currentUser = JSON.parse(u);
      
    }

    this.login = function(data) {
      
      console.log(data);
      
      $auth.login(data).then(function() {

        return $http.get('public/api/logged');

        // Handle errors
      }, function(error) {
        
        root.logErrors = true;
        
        root.logAlerts = error.data.error;
        $log.error = error.data.error;
        //$rootScope.errors.push( error.data.error );

      }).then(function(response) {

        var user = JSON.stringify(response.data.user);

        localStorage.setItem('user', user);

        $rootScope.logged = true;

        $rootScope.currentUser = response.data.user;
        
        $location.path('/users');

      });
    }

    this.signup = function(data) {

      $http.post('public/api/users', data).success(function(success) {

        var registerlog = {
          
          password: data.password,
          
          email: data.email
          
        };
        
        console.log(registerlog);
        
        root.regErrors = true;
        
        root.regAlerts  = success;
        //console.log(root.alerts);
        root.login(registerlog);

      }).error(function(error) {
        
         $rootScope.errors =  error ;
        
      });

    }

    this.logout = function() {

      $auth.logout().then(function() {

        localStorage.removeItem('user');

        $rootScope.logged = false;

        $rootScope.currentUser = null;

        $location.path('/home');
        
      });
    }

    this.claim = function() {
      
      var user = angular.fromJson($window.atob($window.localStorage.session.split('.')[1]));
      
      console.log(user);
      
      return user;
      
    }

  });

})();