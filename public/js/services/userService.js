(function() {

  'use strict';

  angular.module('app')

  .service('userService', function($rootScope, $http, $log, $auth, $location) {

    var root = this;
    root.alerts = {};
   // root.alerts.success = {"webela" : "figa"};
    
   // root.alerts.errors =  {"webela" : "error"};
    
    root.errors;

    this.getList = function() {

      $http.get('public/api/users')

      .then(function(users) {

        root.list = users.data;
        
        console.log(users);
        
        return root.list;

      }).catch(function(error) {
        
        root.alerts = {};

        root.alerts.errors = error.data.error;
        
        console.log(error)

        return root.errors;

      });

    }    
    
    
    this.show = function(id) {

      $http.get('public/api/users/'+ id )

      .then(function(user) {
        
        root.user = user.data;

      }).catch(function(error) {
        
        root.alerts = {};
        
        root.alerts.errors = error.data.error;
        
        $log.error(error);

      });

    }    
    
    this.update = function(id, param) {

      $http.put('public/api/users/'+ id, param )

      .then(function(user) {
        
       root.alerts = {};
        
       root.alerts.success = user.data.success;
        
       console.log(user.data);

      }).catch(function(error) {

        root.alerts = {};
        
        root.alerts.errors = error.data.error;
        
        console.log(error);

      });

    }

  });

})();