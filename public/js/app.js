//http://jonsamwell.com/url-route-authorization-and-security-in-angular/
(function() {

  'use strict';

  angular.module('app', ['ngRoute', 'ngAnimate', 'ngResource', 'ngStorage', 'satellizer', 'ui.bootstrap'])

  .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

    $authProvider.loginUrl = 'childapp/public/api/login';
    
    $authProvider.facebook({
      clientId: '1576033746058865'
    });

    $routeProvider.when('/home', {

      templateUrl: 'view/home.html',

    }).when('/users', {

      templateUrl: 'view/users.html',

      controller: 'userController'

    }).when('/posts', {

      templateUrl: 'view/posts.html',

    }).when('/tags', {

      templateUrl: 'view/tag.html',

    }).otherwise({

      redirectTo: '/home'

    });

  }]).config(['$httpProvider', function($httpProvider) {

    $httpProvider.interceptors.push(['$rootScope', '$q', '$location',

      function($rootScope, $q, $location) {

        return {

          'request': function(config) {

            config.headers = config.headers || {};

            if (localStorage.session) {

              config.headers.Authorization = 'Bearer ' + localStorage.session;

            }

            return config;

          },

          'response': function(res) {

            if (res.data.status === 401 || res.data.status === 500) {

              $location.path('/api/login');

              $location.replace();
            }

            return res || $q.when(res);
          }
        };

      }

    ]);

  }]);

})();