(function() {

'use strict';

angular.module('app')
  .controller('siteController', function($rootScope, $scope, $http, $window, $location, $log, authService, $uibModal) {
    // toogle hide show for ng-repeat 
    // $scope.username = $window.localStorage.getItem('user');
    // $scope.u = $rootScope.currentUser ;
    $scope.toggleitem = false;
    // toogle hide show  
    $scope.toggle = false;
  
    function getLocation(){
      
       if (navigator.geolocation) {
         
          navigator.geolocation.getCurrentPosition(showPosition);
         
      } else {
        
          $scope.position = "Geolocation is not supported by this browser.";
      }
      
    } 
    
    function showPosition(position) {
      
      $scope.position =  position.coords.latitude + 
        
      "," + position.coords.longitude; 
      
    }
  getLocation();

    $scope.test = function() {

      $http.get('public/api/reset/bolladaniele@gmail.com').then(function(data) {

        $log.info(data);

      }).catch(function(error) {

        $log.error(error);

      });
    }

    $scope.open = function(size) {

     $uibModal.open({
        animation:true,
        templateUrl: 'view/form/email.html',
        size: size,
      });

    };

  });
  
  
})();